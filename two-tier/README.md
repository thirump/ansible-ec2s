# Basic Two-Tier AWS Architecture - Ansible EC2s - Test:x


This provides a template for running a simple two-tier architecture on Amazon
Web services. The premise is that you have stateless app servers running behind
an ELB serving traffic.
