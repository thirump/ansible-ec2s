#!/bin/bash

function set_hostname()  {
   HOSTNAME=tomcat-dev
   hostname $HOSTNAME
   echo "HOSTNAME=$HOSTNAME" > /etc/hostname
   echo "HOSTNAME=$HOSTNAME" >> /etc/sysconfig/network
   hostnamectl set-hostname $HOSTNAME --static
   echo "preserve_hostname: true" >> /etc/cloud/cloud.cfg
}

function install_tomcat() {
    sudo yum update -y
    sudo yum install -y tomcat
    sudo systemctl start tomcat
    sudo systemctl enable tomcat
}

set_hostname
install_tomcat