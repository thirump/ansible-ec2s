output "address" {
  value = "${aws_elb.web.dns_name}"
}

output "instance_ips" {
  value = ["${aws_instance.tomcat.*.public_ip}"]
}